/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package histogramspecification;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import java.util.*; 

/**
 *
 * @author DELL
 */
public class HistogramSpecification {

    /**
     * @param args the command line arguments
     */
    
    DecimalFormat numberFormat = new DecimalFormat("0.0000000000");
    public static void main(String[] args) {
        new HistogramSpecification();
    }
    
    public HistogramSpecification(){
        int[][] PDF = new int[3][256];
        try {
            BufferedImage image = ImageIO.read(this.getClass().getResource("b.jpg"));
            Equalization(image);
            
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private void Equalization(BufferedImage image) {
        int w = image.getWidth();
        int h = image.getHeight();
        double[] RK = new double[256];
        double[] PDFred;
        double[] PDFgreen;
        double[] PDFblue;
        double[] TRred;
        double[] PSred;
        int[] PRred;
        int[][] matrixRed = new int[h][w];
        int[][] matrixGreen = new int[h][w];
        int[][] matrixBlue = new int[h][w];



        for (int i = 0; i < h; i++) {
          for (int j = 0; j < w; j++) {
            int pixel = image.getRGB(j, i);
            matrixRed[i][j]   = getRed(pixel);
          }
        }

        for (int i = 0; i < 256; i++) {
            RK[i] = (double)i / (double)255;
        }

        PDFred = getPDF(matrixRed, h, w);
        TRred = getTR(PDFred);
        PRred = getPR(TRred, RK);
        PSred = getPS(PRred, matrixRed, h, w);
    }

    private double[] getPS(int[] pRred, int[][] m, int h, int w) {
        ArrayList<Integer> temp = new ArrayList<>();
        int[] nk = new int[256];
        double[] ps = new double[256];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                temp.add(m[i][j]);
            }
        }
        Map<Integer, Integer> hm = new HashMap<Integer, Integer>();
        for (Integer i : temp) {
            Integer j = hm.get(i);
            hm.put(i, (j == null) ? 1 : j + 1);
        }
        for (Map.Entry<Integer, Integer> val : hm.entrySet()) {
            nk[val.getKey()] = val.getValue();
        }

        int prTerakhir = pRred[0];

        for(int i = 0; i < pRred.length; i++){
            if(prTerakhir != pRred[i]){
                ps[pRred[i]] = nk[i];
                prTerakhir = pRred[i];
            } else if(prTerakhir == pRred[i]){
                ps[pRred[i]] += nk[i];
            }
        }
//        for (int i = 0; i < nk.length; i++){
//            System.out.println(i + " nk " + nk[i]);
//        }
//        for (int i = 0; i < nk.length; i++){
//            System.out.println(i + " ps " + ps[i]);
//        }
        return ps;
    };

    int getAlpha(int pixel){
        int alpha = (pixel >> 24) & 0xff;
        return alpha;
    }

    int getRed(int pixel){
        int red = (pixel >> 16) & 0xff;
        return red;
    }

    int getGreen(int pixel){
        int green = (pixel >> 8) & 0xff;
        return green;
    }

    int getBlue(int pixel){
        int blue = (pixel) & 0xff;
        return blue;
    }

    int getGreyScale(int pixel){
        int gs = (int)(getRed(pixel)+getGreen(pixel)+getBlue(pixel)) / 3;
        return gs;
    }

    private double[] getPDF(int[][] m, int h, int w) {
        ArrayList<Integer> temp = new ArrayList<>();
        double[] PDF = new double[256];


            for (int i = 0; i < h; i++) {
                for (int j = 0; j < w; j++) {
                    temp.add(m[i][j]);
                }
             }


        Map<Integer, Integer> hm = new HashMap<Integer, Integer>();
        for (Integer i : temp) {
            Integer j = hm.get(i);
            hm.put(i, (j == null) ? 1 : j + 1);
        }



        for (Map.Entry<Integer, Integer> val : hm.entrySet()) {
            PDF[val.getKey()] = (double)val.getValue() / (double)(w*h);
        }
        return PDF;
    }

    private double[] getTR(double[] PDF) {
        double[] TR = new double[256];
        double temp = 0;
        for (int i = 0; i < 256; i++) {
            temp += PDF[i];
            TR[i] = temp;
        }
        return TR;
    }

    private int[] getPR(double[] TR, double[] RK) {
        int[] PR = new int[256];
        int temp = 0;
        for (int i = 0; i < 256; i++) {
            double trSekarang = TR[i];
            double jarak = Math.abs(trSekarang - RK[0]);
            int idx = 0;
            for(int c = 1; c < RK.length; c++){
                double jarakC = Math.abs(RK[c] - trSekarang);
                if(jarakC < jarak){
                    idx = c;
                    jarak = jarakC;
                }
            }
//            System.out.println("IDX " + idx);
            PR[i] = idx;
        }
        return PR;
    }
}
    